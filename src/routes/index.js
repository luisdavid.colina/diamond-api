const { Router } = require('express');
const router = Router();

const { getUsers, getUserById, getChkById } = require('../controllers/index.controller');

router.get('/users', getUsers);
router.get('/users/:id', getUserById);
router.get('/chk/:id', getChkById);


module.exports = router;