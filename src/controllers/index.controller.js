const { Pool } = require('pg');
const fs = require('fs')
const { spawn } = require('child_process');

const pool = new Pool({
    user: 'diamond',
    host: 'diamondhotelsystems.online',
    database: 'hotel',
    password: 'lksdfgj53fd',
    port: 5432,
});

const getUsers = async (req, res) => {
    const response = await pool.query('SELECT * FROM master.tbl_clientes');
    res.status(200).json(response.rows);
};

const getUserById = async (req, res) => {
    const id = parseInt(req.params.id);
    const query = "SELECT master.tbl_clientes.nombre AS nombre_cliente, master.tbl_clientes.apellido AS apellido_cliente, master.tbl_habitaciones.id, master.tbl_reservas_detalle.check_in_fecha, master.tbl_reservas_detalle.check_out_fecha, master.tbl_reservas_detalle.deleted_at, master.tbl_habitaciones.piso, master.tbl_habitaciones.numero, master.tbl_habitaciones.personas_minimo, master.tbl_habitaciones.personas_maximo, master.tbl_habitaciones_tipo.nombre as tipo_habitacion, master.tbl_reservas_detalle.id_reservas_grupo, master.tbl_reservas_grupo.id_reservas_estado, master.tbl_reservas_grupo.id_reservas, master.tbl_reservas_detalle.id_habitacion_tipo from master.tbl_reservas_detalle INNER JOIN master.tbl_habitaciones on master.tbl_reservas_detalle.id_habitacion = master.tbl_habitaciones.id INNER JOIN master.tbl_habitaciones_tipo on master.tbl_habitaciones.id_habitacion_tipo = master.tbl_habitaciones_tipo.id INNER JOIN master.tbl_reservas_grupo on master.tbl_reservas_detalle.id_reservas_grupo = master.tbl_reservas_grupo.id INNER JOIN master.tbl_reservas on master.tbl_reservas_grupo.id_reservas = master.tbl_reservas.id INNER JOIN master.tbl_clientes on master.tbl_reservas.id_cliente = master.tbl_clientes.id WHERE master.tbl_habitaciones.deleted_at is null and master.tbl_reservas_detalle.deleted_at is null and master.tbl_reservas_detalle.deleted_at is null and master.tbl_habitaciones.numero = $1 and (DATE(master.tbl_reservas_detalle.check_in_fecha) <= CURRENT_DATE and CURRENT_DATE <= DATE(master.tbl_reservas_detalle.check_out_fecha)) GROUP BY master.tbl_clientes.id, master.tbl_clientes.nombre, master.tbl_clientes.apellido, master.tbl_habitaciones.id, master.tbl_reservas_detalle.check_in_fecha, master.tbl_reservas_detalle.check_out_fecha, master.tbl_reservas_detalle.deleted_at, master.tbl_habitaciones.piso, master.tbl_habitaciones.numero, master.tbl_habitaciones.personas_minimo, master.tbl_habitaciones.personas_maximo, master.tbl_habitaciones_tipo.nombre, master.tbl_reservas_detalle.id_reservas_grupo, master.tbl_reservas_grupo.id_reservas_estado, master.tbl_reservas_grupo.id_reservas, master.tbl_reservas_detalle.id_habitacion_tipo ORDER BY DATE(master.tbl_reservas_detalle.check_in_fecha) ASC, DATE(master.tbl_reservas_detalle.check_out_fecha) DESC LIMIT 1";
    const response = await pool.query(query, [id]);
    res.json(response.rows);
};

const getChkById = async (req, res) => {
    const id = parseInt(req.params.id);
    const query = "SELECT master.tbl_clientes.nombre AS nombre_cliente, master.tbl_clientes.apellido AS apellido_cliente, master.tbl_habitaciones.id, master.tbl_reservas_detalle.check_in_fecha, master.tbl_reservas_detalle.check_out_fecha, master.tbl_reservas_detalle.deleted_at, master.tbl_habitaciones.piso, master.tbl_habitaciones.numero, master.tbl_habitaciones.personas_minimo, master.tbl_habitaciones.personas_maximo, master.tbl_habitaciones_tipo.nombre as tipo_habitacion, master.tbl_reservas_detalle.id_reservas_grupo, master.tbl_reservas_grupo.id_reservas_estado, master.tbl_reservas_grupo.id_reservas, master.tbl_reservas_detalle.id_habitacion_tipo from master.tbl_reservas_detalle INNER JOIN master.tbl_habitaciones on master.tbl_reservas_detalle.id_habitacion = master.tbl_habitaciones.id INNER JOIN master.tbl_habitaciones_tipo on master.tbl_habitaciones.id_habitacion_tipo = master.tbl_habitaciones_tipo.id INNER JOIN master.tbl_reservas_grupo on master.tbl_reservas_detalle.id_reservas_grupo = master.tbl_reservas_grupo.id INNER JOIN master.tbl_reservas on master.tbl_reservas_grupo.id_reservas = master.tbl_reservas.id INNER JOIN master.tbl_clientes on master.tbl_reservas.id_cliente = master.tbl_clientes.id WHERE master.tbl_habitaciones.deleted_at is null and master.tbl_reservas_detalle.deleted_at is null and master.tbl_reservas_detalle.deleted_at is null and master.tbl_habitaciones.numero = $1 and (DATE(master.tbl_reservas_detalle.check_in_fecha) <= CURRENT_DATE and CURRENT_DATE <= DATE(master.tbl_reservas_detalle.check_out_fecha)) GROUP BY master.tbl_clientes.id, master.tbl_clientes.nombre, master.tbl_clientes.apellido, master.tbl_habitaciones.id, master.tbl_reservas_detalle.check_in_fecha, master.tbl_reservas_detalle.check_out_fecha, master.tbl_reservas_detalle.deleted_at, master.tbl_habitaciones.piso, master.tbl_habitaciones.numero, master.tbl_habitaciones.personas_minimo, master.tbl_habitaciones.personas_maximo, master.tbl_habitaciones_tipo.nombre, master.tbl_reservas_detalle.id_reservas_grupo, master.tbl_reservas_grupo.id_reservas_estado, master.tbl_reservas_grupo.id_reservas, master.tbl_reservas_detalle.id_habitacion_tipo ORDER BY DATE(master.tbl_reservas_detalle.check_in_fecha) ASC, DATE(master.tbl_reservas_detalle.check_out_fecha) DESC LIMIT 1";
    const response = await pool.query(query, [id]);

    let text = '<?xml version="1.0"?> <CallCenterRequest> <AddOrder> <Order><Customer> <FirstName>HAB';
    text += id;
    text += '</FirstName><LastName>PMS</LastName> <AddressLine1>Surquillo</AddressLine1><City>Lima</City> <State>Lima</State> <ZipCode>Some Zip</ZipCode><EmailAddress>anael.brito@ncr.com</EmailAddress><PhoneNumber>555-555-0000</PhoneNumber><Notes>Some Notes</Notes> </Customer><Mode>Delivery</Mode> </Order> </AddOrder></CallCenterRequest>'

    fs.writeFileSync("/pcnub/XML/xml.xml", text);
    
    const bat = spawn('cmd.exe', ['/c', 'C:\\pcnub\\INYECTAR.bat']);

    bat.stdout.on('data', (data) => {
        console.log(data.toString());
      });
      
      bat.stderr.on('data', (data) => {
        console.error(data.toString());
      });
      
      bat.on('exit', (code) => {
        console.log(`Child exited with code ${code}`);
      });
    
    res.json(response.rows);
};

module.exports = {
    getUsers, getUserById, getChkById,
};